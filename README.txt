### Instrucciones de Instalación y Uso

### Clonar Repositorio
-git clone https://gitlab.com/AnBonilla/agenda.git
-cd RepoBook

### Backend (Laravel)
### Instalar Dependencias
-cd api_address
-composer install

### Configurar Entorno
## Copiar el archivo de configuración de ejemplo:
-cp .env.example .env
-Configurar la base de datos en .env con tus credenciales locales.

### Migraciones y Seeder
-php artisan migrate --seed
-php artisan db:seed --class=ContactsTableSeeder

### Levantar Servidor
-php artisan serve
-El backend estará disponible en http://localhost:8000

### Frontend (Angular)
## Instalar Dependencias
-cd contact-book
-npm install
### Levantar Servidor de Desarrollo
-npm start
-El frontend estará disponible en http://localhost:4200.

### Uso
-Para utilizar la aplicación, accede a http://localhost:4200 en tu navegador. 
-Podrás gestionar contactos, añadir nuevos, editar existentes y ver detalles de cada contacto.

### Consideraciones
-Asegúrate de tener Node.js y npm instalados para ejecutar el frontend de Angular.
-Verifica que tu entorno cumpla con los requisitos de Laravel antes de iniciar el backend.
-Ajusta las configuraciones de base de datos y otros parámetros según tu entorno de desarrollo.