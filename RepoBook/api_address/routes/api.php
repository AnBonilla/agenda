<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\ContactController;

Route::apiResource('contacts', ContactController::class);
Route::delete('contacts/{contact}/related/{relatedType}/{relatedId}', [ContactController::class, 'destroyRelated']);

Route::get('/hola', function () {
    return response()->json(['message' => '¡Hola!']);
});

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');
