<?php

namespace Database\Factories;
use App\Models\Contact;
use App\Models\Phone;
use App\Models\Email;
use App\Models\Address;

use Illuminate\Database\Eloquent\Factories\Factory;

class ContactFactory extends Factory
{
    protected $model = Contact::class;
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
        ];
    }
    public function configure()
    {
        return $this->afterCreating(function (Contact $contact) {
            $contact->phones()->saveMany(Phone::factory()->count(2)->make(['contact_id' => $contact->id]));
            $contact->emails()->saveMany(Email::factory()->count(2)->make(['contact_id' => $contact->id]));
            $contact->addresses()->saveMany(Address::factory()->count(2)->make(['contact_id' => $contact->id]));
        });
    }
}
