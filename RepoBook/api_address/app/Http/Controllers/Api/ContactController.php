<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\Phone;
use App\Models\Email;
use App\Models\Address;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        $query = Contact::with(['phones', 'emails', 'addresses']);

        if ($request->has('search')) {
            $search = $request->get('search');
            $query->where(function ($q) use ($search) {
                $q->where('name', 'like', "%{$search}%")
                    ->orWhereHas('phones', function ($q) use ($search) {
                        $q->where('number', 'like', "%{$search}%");
                    })
                    ->orWhereHas('emails', function ($q) use ($search) {
                        $q->where('email', 'like', "%{$search}%");
                    })
                    ->orWhereHas('addresses', function ($q) use ($search) {
                        $q->where('address', 'like', "%{$search}%");
                    });
            });
        }

        // Paginación: número de elementos por página
        $perPage = $request->has('perPage') ? (int) $request->get('perPage') : 10;

        // Obtener los resultados paginados
        $contacts = $query->paginate($perPage);

        return response()->json($contacts);
    }

    // Mostrar un contacto específico
    public function show($id)
    {
        $contact = Contact::with(['phones', 'emails', 'addresses'])->findOrFail($id);
        return response()->json($contact);
    }

    // Crear un nuevo contacto
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'phones' => 'array',
            'phones.*.number' => 'required|string|max:255',
            'emails' => 'array',
            'emails.*.email' => 'required|email|max:255',
            'addresses' => 'array',
            'addresses.*.address' => 'required|string|max:255',
        ]);

        $contact = Contact::create($request->only('name'));

        if ($request->has('phones')) {
            foreach ($request->phones as $phoneData) {
                $contact->phones()->create($phoneData);
            }
        }

        if ($request->has('emails')) {
            foreach ($request->emails as $emailData) {
                $contact->emails()->create($emailData);
            }
        }

        if ($request->has('addresses')) {
            foreach ($request->addresses as $addressData) {
                $contact->addresses()->create($addressData);
            }
        }

        return response()->json($contact->load(['phones', 'emails', 'addresses']), 201);
    }

    // Actualizar un contacto existente
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'phones' => 'array',
            'phones.*.number' => 'required|string|max:255',
            'emails' => 'array',
            'emails.*.email' => 'required|email|max:255',
            'addresses' => 'array',
            'addresses.*.address' => 'required|string|max:255',
        ]);

        $contact = Contact::findOrFail($id);
        $contact->update($request->only('name'));

        $contact->phones()->delete();
        if ($request->has('phones')) {
            foreach ($request->phones as $phoneData) {
                $contact->phones()->create($phoneData);
            }
        }

        $contact->emails()->delete();
        if ($request->has('emails')) {
            foreach ($request->emails as $emailData) {
                $contact->emails()->create($emailData);
            }
        }

        $contact->addresses()->delete();
        if ($request->has('addresses')) {
            foreach ($request->addresses as $addressData) {
                $contact->addresses()->create($addressData);
            }
        }

        return response()->json($contact->load(['phones', 'emails', 'addresses']), 200);
    }

    // Eliminar un contacto
    public function destroy($id)
    {
        $contact = Contact::findOrFail($id);
        $contact->delete();

        return response()->json(null, 204);
    }

    // Eliminar un teléfono, email o dirección
    public function destroyRelated($contactId, $relatedType, $relatedId)
    {
        $contact = Contact::findOrFail($contactId);

        switch ($relatedType) {
            case 'phone':
                $contact->phones()->findOrFail($relatedId)->delete();
                break;
            case 'email':
                $contact->emails()->findOrFail($relatedId)->delete();
                break;
            case 'address':
                $contact->addresses()->findOrFail($relatedId)->delete();
                break;
            default:
                return response()->json(['message' => 'Invalid related type'], 400);
        }

        return response()->json(null, 204);
    }
}
