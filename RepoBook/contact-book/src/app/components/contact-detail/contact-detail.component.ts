import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContactService } from '../../services/contact.service';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.css']
})
export class ContactDetailComponent implements OnInit {
  contact: any;

  constructor(private route: ActivatedRoute, private contactService: ContactService) { }

  ngOnInit(): void {
    const contactId = this.route.snapshot.params["id"];
    this.contactService.getContact(contactId).subscribe(response => {
      this.contact = response;
    });
  }
}
