import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ContactService } from '../../services/contact.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit {
  contactForm: FormGroup;
  contactId: number;
  serverErrors: any = {};
  private unsubscribe$ = new Subject<void>();
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private contactService: ContactService
  ) {
    this.contactForm = this.fb.group({
      name: ['', Validators.required],
      emails: this.fb.array([]),
      phones: this.fb.array([]),
      addresses: this.fb.array([])
    });

    this.contactId = this.route.snapshot.params['id'];
  }

  ngOnInit(): void {
    this.contactService.getContact(this.contactId).subscribe(contact => {
      this.contactForm.patchValue({
        name: contact.name
      });

      contact.emails.forEach((email: any) => this.emails.push(this.createEmailField(email)));
      contact.phones.forEach((phone: any) => this.phones.push(this.createPhoneField(phone)));
      contact.addresses.forEach((address: any) => this.addresses.push(this.createAddressField(address)));
    });
  }

  get phones(): FormArray {
    return this.contactForm.get('phones') as FormArray;
  }

  get addresses(): FormArray {
    return this.contactForm.get('addresses') as FormArray;
  }

  get emails(): FormArray {
    return this.contactForm.get('emails') as FormArray;
  }

  createEmailField(email: any = {}): FormGroup {
    return this.fb.group({
      email: [email.email || '', [Validators.required, Validators.email]]
    });
  }

  createPhoneField(phone: any = {}): FormGroup {
    return this.fb.group({
      number: [phone.number || '', Validators.required]
    });
  }

  createAddressField(address: any = {}): FormGroup {
    return this.fb.group({
      address: [address.address || '', Validators.required]
    });
  }

  addEmailField(): void {
    this.emails.push(this.createEmailField());
  }

  addPhoneField(): void {
    this.phones.push(this.createPhoneField());
  }

  addAddressField(): void {
    this.addresses.push(this.createAddressField());
  }

  removeEmailField(index: number): void {
    this.emails.removeAt(index);
  }

  removePhoneField(index: number): void {
    this.phones.removeAt(index);
  }

  removeAddressField(index: number): void {
    this.addresses.removeAt(index);
  }

  saveContact(): void {
    if (this.contactForm.valid) {
      this.contactService.updateContact(this.contactId, this.contactForm.value).subscribe(() => {
        this.router.navigate(['/contacts']);
      });
    } else {
      this.contactForm.markAllAsTouched();
    }
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
