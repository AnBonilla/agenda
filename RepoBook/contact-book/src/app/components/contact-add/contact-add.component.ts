import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ContactService } from '../../services/contact.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.css']
})
export class ContactAddComponent {
  contactForm: FormGroup;
  serverErrors: any = {};
  private unsubscribe$ = new Subject<void>();
  constructor(private router: Router, private fb: FormBuilder, private contactService: ContactService) {
    this.contactForm = this.fb.group({
      name: ['', Validators.required],
      emails: this.fb.array([this.createEmailField()]),
      phones: this.fb.array([this.createPhoneField()]),
      addresses: this.fb.array([this.createAddressField()])
    });
  }

  get phones(): FormArray {
    return this.contactForm.get('phones') as FormArray;
  }

  get addresses(): FormArray {
    return this.contactForm.get('addresses') as FormArray;
  }

  get emails(): FormArray {
    return this.contactForm.get('emails') as FormArray;
  }

  createEmailField(): FormGroup {
    return this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  createPhoneField(): FormGroup {
    return this.fb.group({
      number: ['', Validators.required]
    });
  }

  createAddressField(): FormGroup {
    return this.fb.group({
      address: ['', Validators.required]
    });
  }

  addEmailField(): void {
    this.emails.push(this.createEmailField());
  }

  addPhoneField(): void {
    this.phones.push(this.createPhoneField());
  }

  addAddressField(): void {
    this.addresses.push(this.createAddressField());
  }

  removeEmailField(index: number): void {
    this.emails.removeAt(index);
  }

  removePhoneField(index: number): void {
    this.phones.removeAt(index);
  }

  removeAddressField(index: number): void {
    this.addresses.removeAt(index);
  }

  addContact(): void {
    if (this.contactForm.valid) {
      this.contactService.addContact(this.contactForm.value).subscribe(() => {
        this.router.navigate(['/contacts']);
      });
    } else {
      this.contactForm.markAllAsTouched();
    }
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
