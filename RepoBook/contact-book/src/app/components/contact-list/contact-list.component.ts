import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContactService } from '../../services/contact.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  contacts: any[] = [];
  search: string = '';
  currentPage: number = 1;
  totalPages: number = 1;
  perPage: number = 10;
  maxVisiblePages: number = 5; // Número máximo de botones de página visibles

  constructor(private router: Router, private contactService: ContactService) { }

  ngOnInit(): void {
    this.loadContacts();
  }

  loadContacts(): void {
    this.contactService.getContacts(this.search, this.currentPage, this.perPage).subscribe(
      (data: any) => {
        this.contacts = data.data;
        this.currentPage = data.current_page;
        this.totalPages = data.last_page;
      },
      error => {
        console.error('Error fetching contacts', error);
      }
    );
  }

  onSearchChange(): void {
    this.currentPage = 1;
    this.loadContacts();
  }

  hasPreviousPage(): boolean {
    return this.currentPage > 1;
  }

  getPreviousPage(): number {
    return this.currentPage - 1;
  }

  hasNextPage(): boolean {
    return this.currentPage < this.totalPages;
  }

  getNextPage(): number {
    return this.currentPage + 1;
  }

  onPageChange(page: number): void {
    this.currentPage = page;
    this.loadContacts();
  }

  deleteContact(id: number): void {
    if (confirm('¿Estás seguro de querer eliminar este contacto?')) {
      this.contactService.deleteContact(id).subscribe(() => {
        this.loadContacts();
      });
    }
  }

  viewContactDetails(id: number): void {
    this.router.navigate(['/contacts', id]);
  }

  editContact(id: number): void {
    this.router.navigate(['/contacts', id, 'edit']);
  }

  getVisiblePages(): number[] {
    const half = Math.floor(this.maxVisiblePages / 2);
    let start = this.currentPage - half;
    let end = this.currentPage + half;

    if (start < 1) {
      start = 1;
      end = this.maxVisiblePages;
    }

    if (end > this.totalPages) {
      end = this.totalPages;
      start = this.totalPages - this.maxVisiblePages + 1;
    }

    if (start < 1) {
      start = 1;
    }

    return Array.from({ length: end - start + 1 }, (_, i) => start + i);
  }
}
