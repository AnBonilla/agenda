import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private baseUrl = 'http://127.0.0.1:8000/api/contacts';

  constructor(private http: HttpClient) { }

  getContacts(search: string = '', page: number = 1, perPage: number = 10): Observable<any> {
    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('perPage', perPage.toString());

    if (search) {
      params = params.append('search', search);
    }

    return this.http.get(this.baseUrl, { params });
  }

  addContact(contact: any): Observable<any> {
    return this.http.post(this.baseUrl, contact);
  }

  updateContact(id: number, contact: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}`, contact);
  }

  deleteContact(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  getContact(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }
}
